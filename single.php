<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if ($post->post_type == 'destinos') {
    
    // Busca os pacotes que são relacionados a este destino
    $pacotes = Timber::get_posts([
        'post_type' => 'pacotes',
        'posts_per_page' => -1,
        'meta_query' => [
            [
                'key' => 'destinos_relacionados',
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE',
            ]
        ]
    ]);
    // Se existir pacotes, adiciona ao contexto
    if ($pacotes) {        
        $context['pacotes'] = $pacotes;
    }


    // Busca os relatos relacionados a este destino
    $relatos = Timber::get_posts([
        'post_type' => 'post',
        'posts_per_page' => -1,
        // 'category_name' => 'relatos-de-clientes',
        'cat' => 37,
        'meta_query' => [
            [
                'key' => 'destinos_relacionados',
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE',
            ]
        ]
    ]);
    // Se existir relatos relacioados, adiciona ao contexto
    if ($relatos) {        
        $context['relatos'] = $relatos;
    }

    // Busca as dicas relacionadas a este destino
    $dicas = Timber::get_posts([
        'post_type' => 'post',
        'posts_per_page' => -1,
        // 'category_name' => 'dicas-de-viagem',
        'cat' => 45,
        'meta_query' => [
            [
                'key' => 'destinos_relacionados',
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE',
            ]
        ]
    ]);
    // Se existir dicas relacionadas, adiciona ao contexto
    if ($dicas) {
        $context['dicas'] = $dicas;
    }

} elseif ($post->post_type == 'pacotes') {
    
    $oportunidades = Timber::get_posts([
        'post_type' => 'post',
        'posts_per_page' => -1,
        'category_name' => 'oportunidades',
        'meta_query' => [
            [
                'key' => 'pacotes_em_destaque',
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE',
            ]
        ]
    ]);

    if ($oportunidades) {
        $context['oportunidades'] = $oportunidades;
    }


    $leiamais = get_posts([
        'post_type' => 'post',
        'posts_per_page' => -1,
        'meta_query' => [
            [
                'key' => 'pacotes_em_destaque',
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE',
            ]
        ]
    ]);

    if ($leiamais) {
        $context['leiamais'] = $leiamais;
    }
    
} elseif ($post->post_type == 'post') {
    
    $lastPosts = Timber::get_posts([
        'post_type' => 'post',
        'posts_per_page' => 3,
    ]);

    if ($lastPosts) {
        $context['lastPosts'] = $lastPosts;
    }
}

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
