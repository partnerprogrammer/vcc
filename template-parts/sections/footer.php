
<?php echo get_template_part('template-parts/sections/newsletter') ?>		

<div class="footer--content text-center text-expand-left">
	<div class="container">
		<div class="row">

			<div class="col-12 col-expand-4 order-expand-2">
				<?php the_field('rodape_secao_1', 'option'); ?>
			</div>

			<div class="col-12 col-expand-12 footer--social order-expand-1">
				<p class="social--title d-expand-none">SIGA</p>
				<ul class="social--list text-left">
					<li>
						<a class="anchor--default d-flex align-items-expand-end" href="<?php the_field('facebook_url', 'option'); ?>" target="_blank" rel="nofollow">
							<span class="social--icon d-flex align-items-expand-end"><i class="d-inline-block icon-fb-white"></i></span>
							<span class="social--content d-none d-expand-inline-block">
								<span class="social--subtitle"><?php the_field('facebook_chamada', 'option'); ?></span>
								<div class="social--tag"><?php the_field('facebook_nome', 'option'); ?></div>
							</span>
						</a>
					</li>
					
					<li>
						<a class="anchor--default d-flex align-items-expand-end" href="<?php the_field('pinterest_url', 'option'); ?>" target="_blank" rel="nofollow">
							<span class="social--icon d-flex align-items-expand-end"><i class="d-inline-block icon-pinte-white"></i></span>
							<span class="social--content d-none d-expand-inline-block">
								<span class="social--subtitle"><?php the_field('pinterest_chamada', 'option'); ?></span>
								<div class="social--tag"><?php the_field('pinterest_nome', 'option'); ?></div>
							</span>
						</a>
					</li>
					
					<li>
						<a class="anchor--default d-flex align-items-expand-end" href="<?php the_field('instagram_url', 'option'); ?>" target="_blank" rel="nofollow">
							<span class="social--icon d-flex align-items-expand-end"><i class="d-inline-block icon-inta-white"></i></span>
							<span class="social--content d-none d-expand-inline-block">
								<span class="social--subtitle"><?php the_field('instagram_chamada', 'option'); ?></span>
								<div class="social--tag"><?php the_field('instagram_nome', 'option'); ?></div>
							</span>
						</a>
					</li>
					
				</ul>
			</div>

			<div class="col-12 col-expand-4 order-expand-2">
				<?php the_field('rodape_secao_2', 'option'); ?>
			</div>

			<div class="col-12 col-expand-4 order-expand-2">
				<?php the_field('rodape_secao_3', 'option'); ?>					
			</div>
		</div>
	</div>
</div>

<div class="footer--copyright text-center">				
	<p class=""> © 2014-<?php echo date('Y') ?> <?php bloginfo('name'); ?>. Todos os direitos reservados.</p>				
</div>

<div id="search-wrapper-modal" class="search-wrapper">	
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<svg width="49" class="js-close-search hover--pointer" height="49" xmlns="http://www.w3.org/2000/svg"><path d="M24.255 23.548L46.172 1.63l.71-.71 1.414 1.414-.71.71-21.917 21.917 21.917 21.917.71.71-1.414 1.415-.71-.711-21.917-21.917-21.21 21.21-.71.71L.92 46.882l.711-.71 21.21-21.21L1.63 3.752l-.71-.71 1.414-1.415.71.711 21.21 21.21z" fill="#4A4A4A" fill-rule="nonzero"/></svg>
		<label for="s" class="sr-only"><?php _e( 'Search', 'twentyeleven' ); ?></label>
		<input type="text" required class="field" name="s" id="s" placeholder="<?php esc_attr_e( ' Pesquisar...', 'twentyeleven' ); ?>" />
		<input type="submit" class="icon-search" />
	</form>	
</div>