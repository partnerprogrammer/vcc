<div class="section section--newsletter pos--rel text-center text-expand-left">    		
	<?php pp_set_bg_image(get_field('newsletter_bg', 'option')); ?>	
	<div class="container">
		<?php _p('p', get_field('newsletter_titulo', 'option'), 'section--newsletter--title') ?>
		<?php echo do_shortcode('[contact-form-7 id="2506" title="Signup Newsletter"]') ?>
	</div>
</div>