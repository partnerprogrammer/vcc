<?php 
/**
 * Template Name: Estatisticas
*/

$args = ['post_type' => 'pacotes', 'posts_per_page' => '-1' ];
$query = new WP_Query($args); 
$count = 1;
if ($query->have_posts()):
    while ($query->have_posts()): $query->the_post();

        // $terms = get_post_meta($post->ID, 'estilo_da_viagem', 1);
        // if ($terms) {
        //     $newTerms = [];
        //     foreach ($terms as $termID) {
        //         $term = get_term_by('id', $termID, 'estilo');
        //         if ($term) {
        //             $newTerms[] = $term->name;
        //         }
        //     }
        //     wp_set_post_terms($post->ID, $newTerms, 'estilo');
        // }
        // echo '<pre>'.print_r($terms,1). '</pre>';
        
        

        $variacoes = get_field('variacoes');
        
        if ($variacoes) {

            foreach ($variacoes as $variacao ) {
            
                $composicoes = $variacao['composicoes'];
                foreach ($composicoes as $composicao ) {
                    echo $count . ' - ' .$composicao['adultos'] . ' | ' . $composicao['criancas'] . ' | ' . $composicao['valor'] . '</br>';
                    $count++;
                }
            }

        }
                
    endwhile;
    die();
endif; wp_reset_postdata();