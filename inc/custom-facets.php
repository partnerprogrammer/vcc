<?php 

/* Página relacionada aos filtros do FacetWp */
/* ----------------------------------------- */


/* Layout */
/* ----------------------------------------- */

    /*
        [facet] => Array
            (
                [name] => archive_destinos
                [label] => Busca Destinos
                [type] => autocomplete
                [source] => post_title
                [placeholder] => Digite o destino que você procura
                [selected_values] => 
            )
    */

    // Adiciona o search icon nos filtros de autocomplete
    add_filter( 'facetwp_facet_html', function( $output, $params ) {	
        if ( 'autocomplete' == $params['facet']['type'] ) {
            $output .= '<i class="d-inline-block icon hover--pointer facetwp-autocomplete-update" onClick="FWP.refresh()"></i>';
        }
        return $output;
    }, 10, 2 );
    
    // Adiciona o search icon nos filtros de autocomplete
    add_filter( 'facetwp_facet_html', function( $output, $params ) {	
        
        $fields = [
            'destinos_meses_recomendados' => ['Mês da viagem', 'calendar'],
            'destinos_idade_minima' => ['Idade da criança mais nova', 'people', 'ano(s)'],
            'destinos_estilo_da_viagem' => ['Estilo da Viagem', 'vacation'],


            'pacotes_meses_recomendados' => ['Mês da viagem', 'calendar'],
            'pacotes_dias_de_viagem' => ['Dias de Viagem', 'ampuleta'],
            'pacotes_estilo_da_viagem' => ['Estilo da Viagem', 'vacation'],
            'pacotes_idade_mimima' => ['Idade da criança mais nova', 'people', 'ano(s)'],
            'pacotes_diferenciais' => ['Diferenciais do Pacote', 'star'],
            
            
            'blog_categorias' => ['Categorias', 'star'],
            'blog_destino' => ['Destino', 'pin-2'],
            'blog_assuntos' => ['Filtrar por Assuntos', 'chat'],
            'blog_epoca' => ['Filtrar Época', 'calendar'],
            'blog_estilo' => ['Filtrar Estilo', 'vacation'],

        ];
        // echo $params['facet']['name'];
        if (!in_array($params['facet']['name'], ['archive_destinos', 'archive_pacotes', 'busca_posts'])) {
            
            $output = add_pre_box($output, [
                'title' => $fields[$params['facet']['name']][0],
                'icon' => $fields[$params['facet']['name']][1],
                'extra' => isset($fields[$params['facet']['name']][2]) ? $fields[$params['facet']['name']][2] : '',
            ], $params);

        }

        // remove placeholder="Max"
        $output = str_replace('placeholder="Máx"', 'placeholder="0"', $output);
        $output = str_replace('placeholder="Max"', 'placeholder="0"', $output);
        return $output;
    }, 20, 2 );   
    
    function add_pre_box($output, $args = [], $params) {
        // echo '<pre>'.print_r($params,1). '</pre>';
        // die();
        $return = '';
                    
            $return .=  '<span class="filter--title">'.$args['title'].'</span>';
            $return .=  '<div class="filter--content">';
                // IF se for um range number
                if ($params['facet']['type'] == 'number_range') {
                    $return .= '<span class="input--qtd">';
                    $return .= '<span class="input--qtd-minus"></span>';
                        $return .= $output;
                        $return .= '<span class="input--qtd-plus"></span>';
                        $return .= '<span class="input--qtd-text">'.$args['extra'].'</span>';
                    $return .= '</span>';
                
                // Para todos os checkboxs, sem ser relacionado aos meses
                } else if (
                    $params['facet']['type'] == 'checkboxes' && 
                    ! in_array($params['facet']['name'],
                    [
                        'pacotes_meses_recomendados',
                        'destinos_meses_recomendados'
                    ])
                ) {
                    $return .= '<div class="checkbox--wrapper">';
                        $return .= '<div class="checkbox--up"><span class="d-inline-block icon-arrow-down"></span></div>';
                            $return .= '<div class="checkbox--content">'.$output.'</div>';
                        $return .= '<div class="checkbox--down"><span class="d-inline-block icon-arrow-down"></div>';
                    $return .= '</div>';
                } else {
                    $return .= $output;
                }
            $return .=  '</div>';
            $return .=  '<div class="filter--selected" data-text="">';
                // $return .= '<span class="filter--selected--text"></span>';
                $return .= '<span class="filter--selected--icon icon-'.$args['icon'].'"></span>';
            $return .=  '</div>';
            $return .=  '<div class="filter--actions">';
                $return .=  '<span class="filter--open icon-arrow-down"></span>';
                $return .=  '<span class="filter--close icon-check-circle" onclick="FWP.refresh()"></span>';
            $return .=  '</div>';            

        return $return;
    }

    
/* ----------------------------------------- Layout */


/* Sorts */
/* ----------------------------------------- */
    
    add_filter( 'facetwp_sort_options', function( $options, $params ) {        
        unset( $options['default'] );
        unset( $options['date_desc'] );
        unset( $options['date_asc'] );
        // echo '<pre>'.print_r($options,1). '</pre>';
        // die();
        return $options;
    }, 10, 2 );

    add_filter( 'facetwp_sort_html', function( $html, $params ) {
        $html = '<select class="facetwp-sort-select">';
        foreach ( $params['sort_options'] as $key => $atts ) {
            $html .= '<option value="' . $key . '">' . $atts['label'] . '</option>';
        }
        $html .= '</select>';
        return $html;
    }, 10, 2 );

/* ----------------------------------------- Sorts */


/* Maps */
/* ----------------------------------------- */

    add_filter( 'facetwp_map_marker_args', function( $args, $post_id ) {        
        $args['icon'] = get_images_url('maps/pin.png');
        return $args;
    }, 10, 2 );

    
    add_filter( 'facetwp_map_init_args', function( $args ) {        
        $args['imagePath'] = get_images_url('maps/m');
        $args['init']['styles'] = json_decode(getMapsStyles());
        $args['init']['disableDefaultUI'] = false;        
        $args['init']['scrollWheel'] = false;
        return $args;
    }, 10 );


/* ----------------------------------------- Maps */

/* Muda o titulo para os termos */
/* ----------------------------------------- */

// Altera o nome de exibição dos termos para o "Título Abreviado"
    add_filter( 'facetwp_index_row', function( $params, $class ) {
        // if ( 'destinos_estilo_da_viagem' == $params['facet_name'] ) {
            if (isset($params['term_id']) && $params['term_id']) {
                $newValue = get_field('titulo_abreviado', 'estilo_'. $params['term_id'] );
                if ($newValue) {
                    $params['facet_display_value'] = $newValue; 
                }                
            }            
        // }
        return $params;
    }, 10, 2 ); 
/* ----------------------------------------- Muda o titulo para os termos */

/* FacetWp */
/* ----------------------------------------- */
    add_filter( 'facetwp_i18n', function( $string ) {
        if ( isset( FWP()->facet->http_params['lang'] ) ) {
            $lang = FWP()->facet->http_params['lang'];

            $translations = array();
            $translations['pt-BR']['load_more']['loading_text'] = 'Carregando...';

            if ( isset( $translations[ $lang ][ $string ] ) ) {
                return $translations[ $lang ][ $string ];
            }
        }

        return $string;
    });    
/* ----------------------------------------- FacetWp */


/* Precos */
/* ----------------------------------------- */

    add_filter( 'facetwp_facet_types', function( $facet_types ) {
        $facet_types['precos'] = new FacetWP_Facet_Prices();
        return $facet_types;
    });

    class FacetWP_Facet_Prices {

        function __construct() {
            $this->label = __( 'Preços', 'fwp' );
        }


        /**
         * Load the available choices
         */
        function load_values( $params ) {
            global $wpdb;

            $facet = $params['facet'];
            $from_clause = $wpdb->prefix . 'facetwp_index f';
            $where_clause = $params['where_clause'];

            // Count setting
            $limit = ctype_digit( $facet['count'] ) ? $facet['count'] : 10;

            $from_clause = apply_filters( 'facetwp_facet_from', $from_clause, $facet );
            $where_clause = apply_filters( 'facetwp_facet_where', $where_clause, $facet );

            $sql = "
            SELECT f.facet_value, f.facet_display_value, f.term_id, f.parent_id, f.depth, COUNT(DISTINCT f.post_id) AS counter
            FROM $from_clause
            WHERE f.facet_name = '{$facet['name']}' $where_clause
            GROUP BY f.facet_value
            ORDER BY f.depth, counter DESC, f.facet_display_value ASC
            LIMIT $limit";

            return $wpdb->get_results( $sql, ARRAY_A );
        }


        /**
         * Generate the output HTML
         */
        function render( $params ) {

            $output = '';
            $facet = $params['facet'];
            $values = (array) $params['values'];
            $selected_values = (array) $params['selected_values'];

            $key = 0;
            foreach ( $values as $key => $result ) {
                $selected = in_array( $result['facet_value'], $selected_values ) ? ' checked' : '';
                $selected .= ( 0 == $result['counter'] && '' == $selected ) ? ' disabled' : '';
                $output .= '<div class="facetwp-link' . $selected . '" data-value="' . esc_attr( $result['facet_value'] ) . '">';
                $output .= esc_html( $result['facet_display_value'] ) . ' <span class="facetwp-counter">(' . $result['counter'] . ')</span>';
                $output .= '</div>';
            }

            return $output;
        }


        /**
         * Return array of post IDs matching the selected values
         * using the wp_facetwp_index table
         */
        function filter_posts( $params ) {
            global $wpdb;

            $output = array();
            $facet = $params['facet'];
            $selected_values = $params['selected_values'];

            $sql = $wpdb->prepare( "SELECT DISTINCT post_id
                FROM {$wpdb->prefix}facetwp_index
                WHERE facet_name = %s",
                $facet['name']
            );

            foreach ( $selected_values as $key => $value ) {
                $results = facetwp_sql( $sql . " AND facet_value IN ('$value')", $facet );
                $output = ( $key > 0 ) ? array_intersect( $output, $results ) : $results;

                if ( empty( $output ) ) {
                    break;
                }
            }

            return $output;
        }


        /**
         * Load and save facet settings
         */
        function admin_scripts() {
    ?>
            <script>
            (function($) {
                wp.hooks.addAction('facetwp/load/precos', function($this, obj) {
                    $this.find('.facet-source').val(obj.source);
                    $this.find('.facet-count').val(obj.count);
                });

                wp.hooks.addFilter('facetwp/save/precos', function(obj, $this) {
                    obj['source'] = $this.find('.facet-source').val();
                    obj['count'] = $this.find('.facet-count').val();
                    return obj;
                });
            })(jQuery);
            </script>
    <?php
        }


        /**
         * Parse the facet selections + other front-facing handlers
         */
        function front_scripts() {
    ?>
        <script>
        (function($) {
            wp.hooks.addAction('facetwp/refresh/precos', function($this, facet_name) {
                var selected_values = [];
                $this.find('.facetwp-link.checked').each(function() {
                    selected_values.push($(this).attr('data-value'));
                });
                FWP.facets[facet_name] = selected_values;
            });

            wp.hooks.addFilter('facetwp/selections/precos', function(output, params) {
                var choices = [];
                $.each(params.selected_values, function(idx, val) {
                    var choice = params.el.find('.facetwp-link[data-value="' + val + '"]').clone();
                    choice.find('.facetwp-counter').remove();
                    choices.push({
                        value: val,
                        label: choice.text()
                    });
                });
                return choices;
            });

            $(document).on('click', '.facetwp-type-precos .facetwp-link:not(.disabled)', function() {
                $(this).toggleClass('checked');
                FWP.autoload();
            });
        })(jQuery);
        </script>
    <?php
        }


        /**
         * Admin settings HTML
         */
        function settings_html() {
    ?>
            <tr>
                <td>
                    <?php _e('Count', 'fwp'); ?>:
                    <div class="facetwp-tooltip">
                        <span class="icon-question">?</span>
                        <div class="facetwp-tooltip-content"><?php _e( 'The maximum number of facet choices to show', 'fwp' ); ?></div>
                    </div>
                </td>
                <td><input type="text" class="facet-count" value="10" /></td>
            </tr>
    <?php
        }
    }

/* ----------------------------------------- Precos */
