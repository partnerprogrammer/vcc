<?php

add_action('init', 'save_currencies_index');
function save_currencies_index() {
    
    $lastUpdate = get_option('currencies_index_last_update');
    if ($lastUpdate != date('d-m-Y')) {
        
        
        $currencies = [ 'usd', 'eur' ];
        if ($currencies) {
            foreach ($currencies as $currency ) {
                $api = wp_remote_get('https://economia.awesomeapi.com.br/'. $currency);
                $cotacoes = json_decode(wp_remote_retrieve_body( $api ));
                
                if ($cotacoes) {
                    $ultimaCotacao = $cotacoes[0]->high;
                    update_option('currencies_index_' . $currency, $ultimaCotacao);
                    update_option('currencies_index_last_update', date('d-m-Y'));
                }
            }
        }
        
    }

}


function create_pacote_indexes( $post_id ) {
    $post_type = get_post_type($post_id);
    if ($post_type != 'pacotes') return;

    
    $variacoes = get_field('variacoes', $post_id);
    
    // Estadia Mínima
    if ($variacoes) {
        $index_estadia_minima = 999;
        foreach ($variacoes as $variacao) {
            $index_estadia_minima = $variacao['duracao_do_roteiro_dias'] <= $index_estadia_minima ? $variacao['duracao_do_roteiro_dias'] : $index_estadia_minima;
            // $index_estadia_minima = $variacao['duracao_do_roteiro_noites'] <= $index_estadia_minima ? $variacao['duracao_do_roteiro_noites'] : $index_estadia_minima;
        }
        update_post_meta($post_id, 'index_estadia_minima', $index_estadia_minima);
    }
    
    // // Menor Valor & Maior Valor
    // if ($variacoes) {
        
    // }

    // echo '<pre>'.print_r($variacoes,1). '</pre>';
    // echo '<pre>'.print_r($index_estadia_minima,1). '</pre>';
    // die();
}

add_action('acf/save_post', 'create_pacote_indexes', 20);


// Change pagination for blogs
function my_home_posts_per_page( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', 10 );
    }
}
add_action( 'pre_get_posts', 'my_home_posts_per_page' );



function do_my_shortcode($shortcode) {
    if (!$shortcode) return false;

    ob_start();
    echo do_shortcode($shortcode);
    $output = ob_get_contents();
    ob_end_clean();

    // Adiciona placeholder nos selects
    $output = str_replace('option value=""', 'option placeholder ', $output);
    
    // Adiciona o custom input aos numbers
    $return = '';
    $return .= '<span class="input--qtd">';
            $return .= '<span class="input--qtd-minus"></span>';            
            $return .= '<span class="input--qtd-plus"></span>';
            $return .= '<span class="input--qtd-text"></span>';        
    $return .= '</span> ';
    $search = '<input type="number"';
    $output = str_replace($search, $return . $search, $output);
    

    return $output; 

}


add_action( 'init', 'wpa4182_init');
function wpa4182_init()
{
    global $wp_taxonomies;

    // The list of labels we can modify comes from
    //  http://codex.wordpress.org/Function_Reference/register_taxonomy
    //  http://core.trac.wordpress.org/browser/branches/3.0/wp-includes/taxonomy.php#L350
    $wp_taxonomies['post_tag']->labels = (object)array(
        'name' => 'Assunto',
        'menu_name' => 'Assuntos',
        'singular_name' => 'Assunto',
        'search_items' => 'Pesquisar assuntos',
        'popular_items' => 'Assuntos populares',
        'all_items' => 'Todos os assuntos',
        'parent_item' => null, // Tags aren't hierarchical
        'parent_item_colon' => null,
        'edit_item' => 'Editar assunto',
        'update_item' => 'Atualizar assunto',
        'add_new_item' => 'Adicionar novo assunto',
        'new_item_name' => 'Novo Assunto',
        'separate_items_with_commas' => 'Separa os assuntos por virgula',
        'add_or_remove_items' => 'Adicionar ou remover assuntos',
        'choose_from_most_used' => 'Escolha um assunto entre os mais usados',
    );

    $wp_taxonomies['post_tag']->label = 'Assuntos';
}

/* Rotas Formulários */
/* ----------------------------------------- */
    add_action( 'rest_api_init', function () {
        register_rest_route( 'formularios', '/listar', array(
            'methods' => 'GET',
            'callback' => 'lista_formularios_site',
        ) );
    } );

    function lista_formularios_site($data) {
        if ($_GET['pwd'] == '2)159p6gUm0M0xU') {
            
            $results = [];
            $args = [
                'post_type' => 'flamingo_inbound', 'posts_per_page' => '-1',
                // Using the date_query to filter posts from last week
                'date_query' => array(
                    array(
                        'after' => '1 week ago'
                    )
                )
            ];
            $query = new WP_Query($args);             
            if ($query->have_posts()):
                while ($query->have_posts()): $query->the_post();
                    $meta = get_post_meta(get_the_ID());
                    if ($meta) { $results[get_the_ID()] = $meta; }
                endwhile;
            endif; wp_reset_postdata();

            return $results;
            
        } else {
            return new WP_Error( 'nao_autorizado', 'Por favor, acesse o endpoint usando o password.', array( 'status' => 404 ) );;
        }
    }
/* ----------------------------------------- Rotas Formulários */



/* Exibe apenas Destinos em Estilos */
/* ----------------------------------------- */
    function taxonomy_destinos_filter($query) {
        if ( !is_admin() && $query->is_main_query() && is_tax('estilo') ) {                        
            $query->set('post_type', 'destinos');
        }
    }
  
    add_action('pre_get_posts','taxonomy_destinos_filter');

/* ----------------------------------------- Exibe apenas Destinos em Estilos */



/* Filtros Faixa Etárias */
/* ----------------------------------------- */
    
    // Exibe apenas destinos na faixa etária
    function taxonomy_faixaetaria_filter($query) {
        if ( !is_admin() && $query->is_main_query() && is_tax('faixaetaria') ) {            
            $query->set('post_type', 'destinos');
        }
    }
  
    add_action('pre_get_posts','taxonomy_faixaetaria_filter', 1, 100);


    // Define a faixa etária certa baseada na idade minima recomendada    
    add_action( 'save_post_destinos', 'set_indexes_para_destinos', 100,3 );

    function set_indexes_para_destinos($post_id, $post, $update) {
        
        $idadeMinimaRecomendada = get_field('idade_minima_recomendada');
        
        if ($idade_minima_recomendada >= 0) {
            
            // Pega os termos das faixas etarias
            $faixas = get_terms(['taxonomy' => 'faixaetaria', 'hide_empty' => false ]);            
            $newTerms = [];

            if ($faixas) {
                foreach ($faixas as $term ) {
                    // Verifica se a idade mínima aplica ao termo
                    $idadeMinima = get_field('idade_minima', $term->taxonomy . '_' . $term->term_id);
                    $idadeMaxima = get_field('idade_maxima', $term->taxonomy . '_' . $term->term_id);
                    // Se aplicar, adiciona o termo ao post
                    if ($idadeMinimaRecomendada >= $idadeMinima && $idadeMinimaRecomendada <= $idadeMaxima) {
                        $newTerms[] = $term->term_id;
                    }
                }
            } // if faixas
            
            // Se existir alguma faixa relacionada, adiciona ao destino
            if ($newTerms) {
                $update = wp_set_post_terms($post_id, $newTerms, 'faixaetaria');                
            }
        }
        
    }

/* ----------------------------------------- Filtros Faixa Etárias */


    

/* Adiciona o item de menu para as funções */
/* ----------------------------------------- */
    
  // Hook for adding admin menus
  add_action('admin_menu', 'wpdocs_unsub_add_pages');
    
  /**
   * Adds a new top-level page to the administration menu.
   */
  function wpdocs_unsub_add_pages() {
      add_menu_page(
          'Funções VCC',
          'Funções VCC',
          'manage_options',
          'vcc-funcoes',
          'vcc_funcoes_sync_destinos_cb'
      );
      
      add_submenu_page( 'vcc-funcoes', 'Indexar Destinos', 'Indexar Destinos', 'manage_options', 'vcc-funcoes');      

  }
  
  /**
   * Roda a função para importas os anunciantes da versão do Carlão
   */
  function vcc_funcoes_sync_destinos_cb() {
    $args = ['post_type' => 'destinos', 'posts_per_page' => '-1' ];
    $updateQuery = new WP_Query($args); 
    if ($updateQuery->have_posts()):
      $i = 1;
      while ($updateQuery->have_posts()): $updateQuery->the_post();
        $update = wp_update_post( [ 'ID' => get_the_ID(), 'post_title' => get_the_title() . '' ] );
        if ($update) {
          echo $i . ' ' . get_the_title() .' atualizado... <br />'; $i++;
        }
      endwhile;
    endif; 
    wp_reset_postdata();    
  }

/* ----------------------------------------- Adiciona o item de menu para as funções */



/**
 * Adiciona a página blog as páginas de categoria
 */
add_filter( 'wpseo_breadcrumb_links', 'wpseo_breadcrumb_add_blog_to_category_terms' );
function wpseo_breadcrumb_add_blog_to_category_terms( $links ) {
    global $post;
    $blogPageID = get_option( 'page_for_posts' );
    $blogBreadcrumbTitle = get_post_meta($blogPageID, '_yoast_wpseo_bctitle', true);
    $title = $blogBreadcrumbTitle ? $blogBreadcrumbTitle : get_the_title(($blogPageID));
    if ( is_category() ) {
        $breadcrumb[] = array(
            'url' => get_permalink( $blogPageID ),
            'text' => $title,
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }
    return $links;
}


add_filter( 'facetwp_assets', function( $assets ) {
    unset( $assets['gmaps'] );
    return $assets;
});
