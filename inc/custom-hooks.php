<?php

// Altera o helper para novo título do Destino
add_filter('gettext','destino_custom_enter_title');
function destino_custom_enter_title( $input ) {

    global $post_type;

    if( is_admin() && 'Digite o título aqui' == $input && 'destinos' == $post_type )
        return 'Título que vai aparecer no campo de busca e no mapa. Ex.: África do Sul.';

    return $input;
}

// Altera o helper para novo título do Destino
add_filter('gettext','pacote_custom_enter_title');
function pacote_custom_enter_title( $input ) {

    global $post_type;

    if( is_admin() && 'Digite o título aqui' == $input && 'pacotes' == $post_type )
        return 'Como vai aparecer na miniatura de chamada do pacote e na busca. Ex: Pacote África do Sul';

    return $input;
}

// Altera o helper para novo título do Post
add_filter('gettext','post_custom_enter_title');
function post_custom_enter_title( $input ) {

    global $post_type;

    if( is_admin() && 'Digite o título aqui' == $input && 'post' == $post_type )
        return 'Como vai aparecer na página do post (h1). Usar um título bom para SEO.';

    return $input;
}



/* ACF */
/* ----------------------------------------- */

    if( function_exists('acf_add_options_page') ) {

        acf_add_options_page([
            'page_title' => 'Padrões',
            'position' => 40,
            'icon_url' => 'dashicons-admin-settings'
        ]);    

    }

    // Adiciona os padrões do Título para consultoria
    function acf_load_titulo_consultoria( $field ) {    
        
        if ($field['type'] != 'select') return $field;

        // reset choices
        $field['choices'] = array();    
        
        // get the textarea value from options page without any formatting
        $choices = get_field('opcoes_de_consultoria', 'option');

        if ($choices) {
            foreach ($choices as $choice) {
                $text = $choice['titulo'];
                $field['choices'][$text] = $text;
            }
        }
        
        // return the field
        return $field;
        
    }

    add_filter('acf/load_field/name=consultoria_titulo', 'acf_load_titulo_consultoria');



    function acf_load_pacote_atencao( $field ) {    
        // echo '<pre>'.print_r($field,1). '</pre>';
        // die();
        // reset choices
        $field['choices'] = array();


        // get the textarea value from options page without any formatting
        $choices = get_field('opcoes_de_atencao', 'option');

        if ($choices) {
            foreach ($choices as $choice) {
                $text = $choice['titulo'];
                $field['choices'][$text] = $text;
            }
        }

        $field['choices']['outro'] = 'Outro';

        // return the field
        return $field;
    
    }

    add_filter('acf/load_field/name=atencao', 'acf_load_pacote_atencao');
    
    
    function acf_load_pacote_condicoes_gerais( $field ) {    
        // echo '<pre>'.print_r($field,1). '</pre>';
        // die();
        // reset choices
        $field['choices'] = array();


        // get the textarea value from options page without any formatting
        $choices = get_field('opcoes_de_condicoes_gerais', 'option');

        if ($choices) {
            foreach ($choices as $choice) {
                $text = $choice['titulo'];
                $field['choices'][$text] = $text;
            }
        }

        $field['choices']['outro'] = 'Outro';

        // return the field
        return $field;
    
    }

    add_filter('acf/load_field/name=condicoes_gerais', 'acf_load_pacote_condicoes_gerais');




/* ----------------------------------------- ACF */


/* Contact Form 7 */
/* ----------------------------------------- */
/**
 * Dynamic Select List for Contact Form 7
 * @usage [select name taxonomy:{$taxonomy} ...]
 * 
 * @param Array $tag
 * 
 * @return Array $tag
 */
function dynamic_select_list_taxonomy( $tag ) {
    
    // Only run on select lists
    if( 'select' !== $tag['basetype'] ) {
        return $tag;
    } else if ( empty( $tag['options'] ) ) {
        return $tag;
    }

    if (substr($tag['options'][0], 0, 8) != 'taxonomy') return $tag;

    $term_args = array();
    // Loop thorugh options to look for our custom options
    foreach( $tag['options'] as $option ) {
        $matches = explode( ':', $option );
        
        if( ! empty( $matches ) ) {            

            switch( $matches[0] ) {

                case 'taxonomy':
                    $term_args['taxonomy'] = $matches[1];
                    break;

                case 'parent':
                    $term_args['parent'] = intval( $matches[1] );
                    break;

            }
        }

    }

    // Ensure we have a term arguments to work with
    if( empty( $term_args ) ) {
        return $tag;
    }

    // Merge dynamic arguments with static arguments
    $term_args = array_merge( $term_args, array(
        'hide_empty' => false,
    ) );

    $terms = get_terms( $term_args );
    // echo '<pre>'.print_r($terms,1). '</pre>';
    // die();

    // Add terms to values
    if( ! empty( $terms ) && ! is_wp_error( $term_args ) ) {

        foreach( $terms as $term ) {
            
            $name_abreviado = get_field('titulo_abreviado', 'estilo_'. $term->term_id );
            $name = $name_abreviado ? $name_abreviado : $term->name;
            $tag['values'][] = $name;

        }

        // echo '<pre>'.print_r($tag['values'],1). '</pre>';
        // die();
    }

    return $tag;

}
add_filter( 'wpcf7_form_tag', 'dynamic_select_list_taxonomy', 10 );

/**
 * Dynamic Select List for Contact Form 7
 * @usage [select name cpt:{$slug_cpt} ...]
 * 
 * @param Array $tag
 * 
 * @return Array $tag
 */
function dynamic_select_list_cpt( $tag ) {
    
    // Only run on select lists
    if( 'select' !== $tag['basetype'] ) {
        return $tag;
    } else if ( empty( $tag['options'] ) ) {
        return $tag;
    }

    $queryArgs = array(
        'posts_per_page' => -1
    );

    if (substr($tag['options'][0], 0, 9) != 'post_type') return $tag;
    // Loop thorugh options to look for our custom options
    foreach( $tag['options'] as $option ) {

        $matches = explode( ':', $option );

        if( ! empty( $matches ) ) {
            switch( $matches[0] ) {

                case 'post_type':
                    $queryArgs['post_type'] = $matches[1];
                    break;

            }
        }

    }

    // Ensure we have a term arguments to work with
    if( empty( $queryArgs ) ) {
        return $tag;
    } 

    $posts = get_posts( $queryArgs );

    // Add terms to values
    if( ! empty( $posts ) ) {
        
        foreach( $posts as $post ) {
            // echo '<pre>'.print_r($tag,1). '</pre>';
            // die();
            $tag['values'][$post->ID] = $post->post_title;

        }

    }
    wp_reset_postdata();

    return $tag;

}
add_filter( 'wpcf7_form_tag', 'dynamic_select_list_cpt', 10 );

/* ----------------------------------------- Contact Form 7 */


add_filter( 'relevanssi_comparison_order', 'rlv_post_type_order' );
function rlv_post_type_order( $order_array ) {
    $order_array = array(
        'destinos' => 0,
        'pacotes' => 1,
        'post' => 2,
        'page' => 3,
    );
    return $order_array;
}

add_filter( 'relevanssi_modify_wp_query', 'rlv_orderby' );
function rlv_orderby( $query ) {
    $query->set( 'orderby', 'post_type' );
    $query->set( 'order', 'ASC' );
    return $query;
}
