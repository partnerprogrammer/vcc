<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive.twig', 'index.twig' );

$context = Timber::get_context();
$context['object'] = get_queried_object();
$context['title'] = 'Archive';
if ( is_day() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'D M Y' );
} else if ( is_month() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'M Y' );
} else if ( is_year() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'Y' );
} else if ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
	array_unshift( $templates, 'category.twig' );
} else if ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	array_unshift( $templates, 'category.twig' );
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} else if ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

$context['posts'] = new Timber\PostQuery();

/* Define o leimais no dicas ou relatos em tags */
/* ----------------------------------------- */
	$object = get_queried_object();
	$context['term'] = new TimberTerm($object->term_id);

	if ($object && get_class($object) == 'WP_Term') {
		
		$taxonomy = $object->taxonomy;

		if (in_array($taxonomy, ['faixaetaria', 'estilo', 'epocas'])) {
			
			// Define a key no qual vai ser buscada
			if ($taxonomy == 'faixaetaria') { $key = 'faixa_etaria'; }
			elseif ($taxonomy == 'estilo') { $key = 'estilo_da_viagem'; }
			elseif ($taxonomy == 'epocas') { $key = 'epoca_da_viagem'; }

			// Busca os relatos relacionados a este destino
			$relatos = Timber::get_posts([
				'post_type' => 'post',
				'posts_per_page' => -1,
				// 'category_name' => 'relatos-de-clientes',
				'cat' => 37,
				'meta_query' => [
					[
						'key' => $key,
						'value' => '"' . $object->term_id . '"',
						'compare' => 'LIKE',
					],
				]
			]);			
		
			// Busca as dicas relacionadas a este destino
			$dicas = Timber::get_posts([
				'post_type' => 'post',
				'posts_per_page' => -1,
				// 'category_name' => 'dicas-de-viagem',
				'cat' => 45,
				'meta_query' => [
					[
						'key' => $key,
						'value' => '"' . $object->term_id . '"',
						'compare' => 'LIKE',
					],
				]
			]);

			// Se existir relatos relacioados, adiciona ao contexto
			if ($relatos) {        
				$context['relatos'] = $relatos;
			}

			// Se existir dicas relacionadas, adiciona ao contexto
			if ($dicas) {
				$context['dicas'] = $dicas;
			}

		}

	}	
	
/* ----------------------------------------- Define o leimais no dicas ou relatos */


Timber::render( $templates, $context );
