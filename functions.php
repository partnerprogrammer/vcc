<?php

// Add Hooks (filters & actions)
include('inc/custom-hooks.php');
include('inc/custom-facets.php');
include('inc/custom-filters.php');




// Adiciona tamanho de thumbs customizáveis
add_action('init', 'add_custom_image_sizes');

function add_custom_image_sizes() {
	add_image_size('thumb-maps', 132, 74, true); 
	add_image_size('thumb-post', 910, 530, true);
	add_image_size('thumb-galeria', 1200, 675, true);
	// add_image_size('imagem-thumb', 800, 600, true);
}

// add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );
// Remove default image sizes here. 
function prefix_remove_default_images( $sizes ) {
	// echo '<pre>'.print_r($sizes,1). '</pre>';
	// die();
	unset( $sizes['small']); // 150px
	unset( $sizes['medium']); // 300px
	unset( $sizes['large']); // 1024px
	unset( $sizes['medium_large']); // 768px
 
 return $sizes;
}

function add_inline_scripts_to_footer() { ?>
	
		<script type="text/javascript">
			<?php // Lazyload https://github.com/verlok/lazyload ?>
			(function (w, d) {
				w.addEventListener('LazyLoad::Initialized', function (e) {
					w.lazyLoadInstance = e.detail.instance;
				}, false);
				var b = d.getElementsByTagName('body')[0];
				var s = d.createElement("script"); s.async = true;
				var v = !("IntersectionObserver" in w) ? "8.16.0" : "10.19.0";
				s.src = "https://cdn.jsdelivr.net/npm/vanilla-lazyload@" + v + "/dist/lazyload.min.js";
				w.lazyLoadOptions = {
					elements_selector: ".lazy",
					// callback_enter: function(element) {
					// 	logElementEvent('ENTERED', element);
					// },
					// callback_set: function(element) {
					// 	logElementEvent('SET', element);
					// },
					// callback_error: function(element) {
					// 	logElementEvent('ERROR', element);
					// 	// element.src = 'https://placeholdit.imgix.net/~text?txtsize=21&txt=Fallback%20image&w=220&h=280';
					// },
				};
				b.appendChild(s);
			}(window, document));
			function logElementEvent(eventName, element) {
				console.log(Date.now(), eventName, element.getAttribute('data-bg'));
			}			
		</script>
	<?php
}
add_action( 'wp_footer', 'add_inline_scripts_to_footer' );


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {     
    
    $path_js = get_stylesheet_directory_uri() . '/assets/js/';
 	$path_css = get_stylesheet_directory_uri() . '/assets/css/';
            		
	wp_deregister_script('jquery');	
	wp_deregister_script( 'wp-embed' );
	
	wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', [], false, true);		
	wp_enqueue_script('gsap', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', ['jquery'], false, true);		
	// wp_enqueue_script('gsap-css-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js', [], false, true);				
	wp_enqueue_script('gsap-css-rule-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSRulePlugin.min.js', [], false, true);				
	wp_enqueue_script('scrollmagic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', [], false, true);				
	wp_enqueue_script('scrollmagic--gsap', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', [], false, true);
	// wp_enqueue_script('scrollmagic--debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', ['scrollmagic'], false, true);				
	wp_enqueue_script('flickty', '//unpkg.com/flickity@2/dist/flickity.pkgd.min.js', [], false, true);				
	wp_enqueue_script('gmaps', '//maps.google.com/maps/api/js?key=AIzaSyAiLB6XGlUNZg9m-BZZ7FlPI0Opq8upZQY', ['jquery'], false, true);
	wp_enqueue_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery'], false, true);
	
	wp_enqueue_script('choices-js', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/scripts/choices.min.js', ['jquery'], false, true);
	wp_enqueue_script('mask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', ['jquery'], false, true);
	wp_enqueue_script('moment-js', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js', ['jquery'], false , true);
	wp_enqueue_script('momentjs-ptbr', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/locale/pt-br.js', ['moment-js'], '2.20.1', true );
	wp_enqueue_script('caleran-js', $path_js.'caleran.min.js', ['jquery', 'moment-js'], false , true);

    wp_enqueue_script('pp-main', $path_js . 'main.js', ['jquery'], false, true);
    // wp_enqueue_script('bootstrap-js','//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', ['jquery'], false, true);
	// wp_enqueue_script('pp-libs', $path_js . 'libs.js', [], false, true);	
    
	wp_localize_script( 'pp-main', 'siteVars', [
		'styleMaps' => getMapsStyles(),
		'iconMaps' => get_images_url('maps/m'),
		'pinMaps' => get_images_url('maps/pin.png'),
		'cidades' => get_stylesheet_directory_uri(). '/assets/estados_cidades.json'
	] );
    
    wp_enqueue_style( 'bootstrap', $path_css . 'bootstrap.css');
    wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto:300,300i,400,500,500i,700,700i|Raleway:700');
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style( 'choices-css', '//cdn.jsdelivr.net/npm/choices.js@4.1.0/public/assets/styles/choices.min.css');
	wp_enqueue_style( 'main', $path_css . 'main.css');
    // wp_enqueue_style( 'libs', $path_css . 'libs.css');
	wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
	
}

function r($value) { return $value; }

// Define o header a ser usado
// add_filter( 'pp_set_header', function() { 
//     return 'header_mobile'; 
// }, 10 );

// Define a classe do header a ser usada
add_filter( 'pp_set_header_class', function(){
    return 'header--mobilefull';
}, 10);

// Define as sections a serem usadas na frontpage
add_filter( 'pp_frontpage_sections', function(){
    return [
        'featuredslider'
    ];
}, 10);


// title, description, image, cta
function post_to_carousel($post, $args = []) {	

	if ($post) {
		
		$return = [];

		$return['link'] = get_permalink($post->ID);
		$return['title'] = $post->post_title;
		$return['description'] = get_field('chamada', $post->ID);		
		$return['image'] = get_field('imagem', $post->ID);
		
		return $return;

	} else {
		return false;
	}
}




function get_destinos($destinos = []) {
	
	if (empty($destinos)) {		
		$destinos = get_posts(['post_type' => 'destinos', 'posts_per_page' => 12 ]);
	} else {
		$destinos = get_posts(['post_type' => 'destinos', 'post__in' => $destinos, 'posts_per_page' => -1 ]);
	}
	
	$return = [];

	if ($destinos) {
		foreach ($destinos as $destino ) {
			$return[] = post_to_carousel($destino);
		}	
	}
	
	return $return;
}

function get_padrao($fieldACF, $selected) {
	$opcoes = get_field($fieldACF, 'option');	
	if ($opcoes) {
		foreach ($opcoes as $opcao ) {
			if ($opcao['titulo'] == $selected) {
				return $opcao['descricao'];
			}
		}	
	}
	return false;
}

function plural($count, $string) {
	if (!$count) {
		return false;
	} elseif ($count == 1) {
		return $count . ' ' . $string;
	} else {
		return $count . ' ' . $string . 's';
	}
}

function get_currencies() {
	return [ 'Real (R$)' => 'BRL', 'Dólar Americano (US$)' => 'USD', 'Euro (€)' => 'EUR' ];
}

function formatar_preco($valor, $moeda) {
	$valor = floatval($valor);
	
	$currencies = get_currencies();	
	$indexUSD = get_option('currencies_index_usd');
	$indexEUR = get_option('currencies_index_eur');	

	if ($valor) {
		$return = '';

		// Verifica qual moeda é o preço base
		if ($moeda == 'BRL') {
			
			$return .= '<span class="BRL">A partir de <span>R$ ' . number_format( $valor, 0, ',', '.' ). '</span></span>';
			$return .= '<span class="USD">A partir de <span>US$ ' . number_format( $valor/$indexUSD, 0, ',', '.' ). '</span></span>';
			$return .= '<span class="EUR">A partir de <span>' . number_format( $valor/$indexEUR, 0, ',', '.' ). '€</span></span>';

			return $return;

		} else {
			
			// Verifica qual index usar
			$index = $moeda == 'USD' ? $indexUSD : $indexEUR;
			// Converte o preço para real
			$valorReal = $valor * $index;

			$return .= '<span class="BRL">A partir de <span>R$ ' . number_format( $valorReal, 0, ',', '.' ). '</span></span>';
			if ($moeda == 'USD') {
				$return .= '<span class="USD">A partir de <span>US$ ' . number_format( $valor, 0, ',', '.' ). '</span></span>';
				$return .= '<span class="EUR">A partir de <span>' . number_format( $valorReal/$indexEUR, 0, ',', '.' ). '€</span></span>';				
			} else {
				$return .= '<span class="USD">A partir de <span>US$ ' . number_format( $valorReal/$indexUSD, 0, ',', '.' ). '</span></span>';
				$return .= '<span class="EUR">A partir de <span>' . number_format( $valor, 0, ',', '.' ). '€</span></span>';				
			}

			return $return;
		}

	} else if ($valor == 0) {
		return 'sob consulta';
	} else {
		return false;
	}	
	
}

function get_facet_order() {
	$order = isset($_GET['_sort']) && $_GET['_sort'] ? $_GET['_sort'] : '';
	return $order;
}


function infomaps($postID = false) {
	if (!$postID) {
		global $post;
		$postID = $post->ID;
	}

	echo '<div id="infomaps-'.$postID.'" class="infomaps">';
        $image = get_field('imagem', $postID);
        if ($image) {
			echo '<div class="infomaps--image">';
                echo '<img src="'.$image['sizes']['thumb-maps'].'" class="img-fluid" />';
            echo '</div>';
        }            
        echo '<div class="infomaps--infos">';
			echo '<p class="title">'.get_the_title($postID).'</p>';
			if (get_post_status($postID) == 'publish') {
				echo '<a href="'.get_permalink($postID).'" title="'.get_the_title($postID).'">Ler mais <i class="d-inline-block icon-more"></i></a>';
			}
		echo '</div>';
		
	echo '</div>';
}

function getMapsStyles() {
	return '[{ "elementType":"geometry", "stylers":[ { "color":"#f5f5f5" } ] }, { "elementType":"labels.icon", "stylers":[ { "visibility":"off" } ] }, { "elementType":"labels.text.fill", "stylers":[ { "color":"#616161" } ] }, { "elementType":"labels.text.stroke", "stylers":[ { "color":"#f5f5f5" } ] }, { "featureType":"administrative.land_parcel", "elementType":"labels.text.fill", "stylers":[ { "color":"#bdbdbd" } ] }, { "featureType":"poi", "elementType":"geometry", "stylers":[ { "color":"#eeeeee" } ] }, { "featureType":"poi", "elementType":"labels.text.fill", "stylers":[ { "color":"#757575" } ] }, { "featureType":"poi.park", "elementType":"geometry", "stylers":[ { "color":"#e5e5e5" } ] }, { "featureType":"poi.park", "elementType":"labels.text.fill", "stylers":[ { "color":"#9e9e9e" } ] }, { "featureType":"road", "elementType":"geometry", "stylers":[ { "color":"#ffffff" } ] }, { "featureType":"road.arterial", "elementType":"labels.text.fill", "stylers":[ { "color":"#757575" } ] }, { "featureType":"road.highway", "elementType":"geometry", "stylers":[ { "color":"#dadada" } ] }, { "featureType":"road.highway", "elementType":"labels.text.fill", "stylers":[ { "color":"#616161" } ] }, { "featureType":"road.local", "elementType":"labels.text.fill", "stylers":[ { "color":"#9e9e9e" } ] }, { "featureType":"transit.line", "elementType":"geometry", "stylers":[ { "color":"#e5e5e5" } ] }, { "featureType":"transit.station", "elementType":"geometry", "stylers":[ { "color":"#eeeeee" } ] }, { "featureType":"water", "elementType":"geometry", "stylers":[ { "color":"#c9c9c9" } ] }, { "featureType":"water", "elementType":"labels.text.fill", "stylers":[ { "color":"#9e9e9e" } ] }]';
}

function format_maps_locations($destinos) {

	if ($destinos) {
		$array = [];
		foreach ($destinos as $postID) {
			$array[] = [
				'title' => get_the_title($postID),
				'postID' => $postID,
				'geo' => get_field('localizacao', $postID)
			];
		}
	}
	return json_encode($array);
}


function list_items($args)  {		
	$queryArgs = ['post_type' => $args['post_type'], 'posts_per_page' => -1 ];
	$query = new WP_Query($queryArgs); 
	if ($query->have_posts()):
		echo '<div class="list-items">';
			echo '<select name="" class="list choicesjs" placeholder="'.$args['placeholder'].'">';
				echo '<option placeholder>'.$args['placeholder'].'</option>';
			while ($query->have_posts()): $query->the_post();
				echo '<option value="'.get_the_permalink().'">'.get_the_title().'</option>';
			endwhile;
			echo '</select>';
		echo '</div>';
	endif; wp_reset_postdata();
	
}