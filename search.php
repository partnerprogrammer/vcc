<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array( 'search.twig', 'archive.twig', 'index.twig' );

$context          = Timber::get_context();

if ($wp_query->found_posts) {
    $context['title'] = 'Exibindo resultados para: <strong>' . $wp_query->query['s'] . '</strong>';    
} else {
    $context['title'] = 'Desculpe, mas não encontramos resultados para a sua busca. Por favor, tente de novo com outros termos.';
}
$context['posts'] = new Timber\PostQuery();

Timber::render( $templates, $context );
